<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Activity 2 - Loops??</title>
</head>
<body>
	<div style="display: flex; justify-content: space-evenly;">
		<div style="text-align: center;">
			<h1>Divisible by 5</h1>
			<?php divisbleBy5(); ?>
		</div>

		<div>
			<h1>Array Manipulation</h1>

			<h3>Add</h3>
			<?php array_push($students, 'Jay') ?>
			<pre><?php print_r($students) ?></pre>
			<pre>Student Count: <?= count($students) ?></pre>
			<?php array_push($students, 'Ryan') ?>
			<pre><?php print_r($students) ?></pre>
			<pre>Student Count: <?= count($students) ?></pre>

			<h3>Remove</h3>
			<?php array_pop($students) ?>
			<pre><?php print_r($students) ?></pre>
			<pre>Student Count: <?= count($students) ?></pre>
		</div>
	</div>

</body>
</html>